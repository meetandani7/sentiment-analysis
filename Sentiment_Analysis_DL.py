import nltk
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer as tv
from sklearn.feature_extraction.text import CountVectorizer as cv
from sklearn.linear_model import LogisticRegression as lr

import pickle

      class Sentiment_Analysis_DL:

        def filter_tweets(tweets, punct):
                tokens = nltk.word_tokenize(tweets)
                filtered = ""
                for i in tokens:
                        if i not in punct:
                                filtered += i + " "
                return filtered
                
                

        if __name__ == "__main__":

                file = open('Tweets.csv')
                tweets_in_csv = pd.read_csv(file) #makedata frame
                tweets_raw = tweets_in_csv.drop(tweets_in_csv.columns[[1, 2, 3, 4]], axis=1)    #dataframes

                tweets=[]                       
                sentiments=[]                   
                        
                for i in range(len(tweets_raw)):
                        s = tweets_raw.iat[i, 0]
                        if s == 0:
                                sentiments.append("0")
                        else:
                                sentiments.append("1")
                        
                        tweets.append(tweets_raw.iat[i, 1])
                       
                vect = cv()
                vect.fit(tweets)
                vectors = vect.transform(tweets)
                                
                lgr=lr()
                lgr.fit(vectors, sentiments)
                y = lgr.predict(vectors[-1])
                print(y)
                with open('lgr_model', 'wb') as f:
                        pickle.dump(lgr, f)

                with open('count_vectorizer', 'wb') as f:
                        pickle.dump(vect, f)
