import pickle
from sklearn.feature_extraction.text import CountVectorizer as cv
from sklearn.linear_model import LogisticRegression as lr
import pandas as pd
import nltk


class run:              

        if __name__ == "__main__":
                
                model = lr()
                        
                with open('lgr_model', 'rb') as f:
                        model = pickle.load(f)

                with open('count_vectorizer', 'rb') as f:
                        vectorizer = pickle.load(f)
                """
                while True:
                        print("Enter the sentence (0 to exit) : ")                      

                        v=[]
                        v.append(input())

                        if v[0] == "0":
                                break
                        
                        word_vector = vectorizer.transform(v)
                        
                        y = model.predict(word_vector)
                        
  
                        if y == "0":
                                print("Negative\n")
                        else:
                                print("Positive\n")

                """

                file=open('test.csv')
                tweets = pd.read_csv(file)
                
                tweets=tweets[['Sentiment', 'SentimentText']]
                #print(tweets)
                v=[]
                for i in range(tweets.shape[0]):
                        v.append(tweets.iat[i, 1])

                vector = vectorizer.transform(v)

                y = model.predict(vector)
                count=0
                for i in range(len(y)):
                        if tweets.iat[i, 0] == int(y[i]):
                                count += 1
                accuracy = count/len(y) * 100
                print("accuracy of linear regression model:")
                print(accuracy)
                
                        
