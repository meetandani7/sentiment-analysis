import pandas as pd
import numpy as np
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical
from collections import Counter
import pickle

class DNN:

        def text_to_vector(text, vocab, word2idx):
                vector = np.zeros(len(vocab))
                for word in text.split(' '):
                        index = word2idx.get(word, None)
                        if index:
                                vector[index] += 1
                return vector

        def build_model(vocab, learning_rate=0.000001):
                # This resets all parameters and variables, leave this here
                tf.reset_default_graph()
                    
                #### Your code ####
                net = tflearn.input_data([None, len(vocab)])
                net = tflearn.fully_connected(net, 5, activation='softmax')
                net = tflearn.fully_connected(net, 2, activation='softmax')
                net = tflearn.regression(net, optimizer='sgd', learning_rate=learning_rate, loss='categorical_crossentropy')
                model = tflearn.DNN(net)
                return model
        
        if __name__ == "__main__":
                #reviews = pd.read_csv('reviews.txt', header=None)
                #labels = pd.read_csv('labels.txt', header=None)
                file=open('test.csv')
                tweets = pd.read_csv(file) #makedata frame
                #tweets = tweets_in_csv.drop(tweets_in_csv.columns[[1, 2, 3, 4]],axis=1)

                reviews=tweets[['SentimentText']]
                labels=tweets[['Sentiment']]
                

                print(type(reviews))
                print(reviews.head())

                total_counts = Counter()

                for idx, row in reviews.iterrows():
                        review = row[0]
                        for word in review.split(' '):
                                total_counts[word] += 1

                print("Total words in data set: ", len(total_counts))
                vocab = sorted(total_counts, key=total_counts.get, reverse=True)[:10000]
                print(vocab[:60])

               
                print(vocab[-1], ': ', total_counts[vocab[-1]])
                print('hii')

                word2idx = {word: index for index, word in enumerate(vocab)}## create the word-to-index dictionary here

                word_vectors = np.zeros((len(reviews), len(vocab)), dtype=np.int_)
                for ii, (_, text) in enumerate(reviews.iterrows()):
                        word_vectors[ii] = text_to_vector(text[0], vocab, word2idx)

                Y = (labels=='positive').astype(np.int_)
                records = len(labels)
                shuffle = np.arange(records)
                np.random.shuffle(shuffle)
                test_fraction = 0.5

                train_split, test_split = shuffle[:int(records*test_fraction)], shuffle[int(records*test_fraction):]
                trainX, trainY = word_vectors[train_split,:], to_categorical(Y.values[train_split], 2)
                testX, testY = word_vectors[test_split,:], to_categorical(Y.values[test_split], 2)

                model = build_model(vocab)
                print('hiii')
                # Training
                model.fit(trainX, trainY, validation_set=0.5, show_metric=True, batch_size=128, n_epoch=3)              

                predictions = (np.array(model.predict(testX))[:,0] >= 0.5).astype(np.int_)
                test_accuracy = np.mean(predictions == testY[:,0], axis=0)
                print("Test accuracy: ", test_accuracy)
                
                
