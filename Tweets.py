import User_Credentials as uc
import tweepy
import nltk

class StdOutListener(tweepy.StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        print(status)
    

class Tweets:

    def binarySearch(arr, x, l, r): 
        while l <= r: 
            mid = l + (r - l)/2; 
            if arr[mid] == x: 
                return mid 
            elif arr[mid] < x: 
                l = mid + 1
            else: 
                r = mid - 1   
        return -1


    def Search(words, x):
        if i in words:
            return 1
        return 0

    def analyse_tweets(stuff, positive_words, negative_words) :
        
        tokenizer = nltk.tokenize.TweetTokenizer()
        pos=0
        neg=0
        neu=0
        for status in stuff :
            sentiment=0
            token=tokenizer.tokenize(status.text)
            
            for i in token:
                try:
                    #p = Tweets.Search(positive_words, i, 0, len(positive_words))
                    #n = Tweets.Search(negative_words, i, 0, len(negative_words))
                    p = 0
                    n = 0
                    
                    for j in positive_words:
                        if j == i:
                            p=1
                            break

                    for j in negative_words:
                        if j == i:
                           n=1
                           break
                        
                    #print(i)
                    if p != 0:
                        sentiment += 1
                    elif n != 0 :
                        sentiment -= 1
                    
                    #logic

                except:
                    print("error")
                    error=1
            
            if sentiment == 0:
                #print("Tweet is Neutral")
                neu+=1
            elif sentiment > 0:
                #print("Tweet is Positive")
                pos+=1
            else:
                #print("Tweet is Negative")
                neg+=1
        percent_pos=pos/(pos+neg+neu)*100
        percent_neg=neg/(pos+neg+neu)*100
        percent_neu=100-(percent_pos + percent_neg)
        print("positive tweets: ", percent_pos)
        print("negative tweets: ", percent_neg)
        print("neutral tweets: ", percent_neu)
        print(pos+neg+neu)
            
    if __name__ == "__main__":
        
        print('Enter the id of twitter account')
        name = input()
        print('Enter the number of tweets to be processed')
        num_tweets = input()
        auth = tweepy.OAuthHandler(uc.CONSUMER_KEY, uc.CONSUMER_KEY_SECRET)
        auth.set_access_token(uc.ACCESS_TOKEN_KEY, uc.ACCESS_TOKEN_KEY_SECRET)
        api = tweepy.API(auth)
        listener = StdOutListener()
        stuff = api.user_timeline(screen_name = name, count = num_tweets, rts_include = True)

        pfile = open("positive-words.txt", "r")
        nfile = open("negative-words.txt", "r")
        positive_words = pfile.read().split('\n')
        negative_words = nfile.read().split('\n')

        #print_lines(positive_words)
        analyse_tweets(stuff, positive_words, negative_words)
