import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer as cv
from sklearn.feature_extraction.text import TfidfVectorizer as tv


class RNN:
    if __name__ == "__main__":

        file = open('Tweets.csv')
        tweets_in_csv = pd.read_csv(file) #makedata frame
        tweets_raw = tweets_in_csv.drop(tweets_in_csv.columns[[1, 2, 3, 4]], axis=1)
        print(tweets_raw.shape)
        trainY=[]
        trainX=[]
        
        for i in range(0, len(tweets_raw), 10):
            s = tweets_raw.iat[i, 0]
            if s == 0:
                trainY.append("0")
            else:
                trainY.append("1")
                            
            trainX.append(tweets_raw.iat[i, 1])

        file=open('test.csv')
        tweets = pd.read_csv(file)
                
        tweets=tweets[['Sentiment', 'SentimentText']]
                #print(tweets)
        testX=[]
        testY=[]
        print(tweets.shape)
        for i in range(0, tweets.shape[0], 100):
            testX.append(tweets.iat[i, 1])
            testY.append(tweets.iat[i, 0])

        #vectorizer
        """vect=cv(max_features=10000)
        vect.fit(trainX)
        trainX = vect.transform(trainX)
        testX = vect.transform(testX)
        """
        trainX = pad_sequences(trainX, maxlen=100, value=0.)
        testX = pad_sequences(testX, maxlen=100, value=0.)

        trainY = to_categorical(trainY, nb_classes=2)
        testY = to_categorical(testY, nb_classes=2)

        print("preprocessing done!")
           
        net = tflearn.input_data([None, 100])
        net = tflearn.embedding(net, input_dim=10000, output_dim=128)
        net = tflearn.lstm(net, 128, dropout=0.8)
        net = tflearn.fully_connected(net, 2, activation='softmax')
        net = tflearn.regression(net, optimizer='adam', learning_rate=0.001, loss='categorical_crossentropy')

        print("net created!")
        
        #training
        model = tflearn.DNN(net, tensorboard_verbose=0)
        model.fit(trainX,  trainY, validation_set=(testX, testY), show_metric=True, batch_size=32)
        print("done")
        
